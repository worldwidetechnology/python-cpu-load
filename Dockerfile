FROM registry.access.redhat.com/ubi7/python-38@sha256:0765c3aacf3d379eb7bd15c10ca72db1ffc2b5601dd2e036b38da59ab33da317
USER root
COPY . /tmp/src
RUN chown -R 1001:0 /tmp/src
USER 1001
RUN /usr/libexec/s2i/assemble
EXPOSE 8080
WORKDIR /opt/app-root/src
CMD ["/bin/sh", "-c", "/usr/libexec/s2i/run"]
